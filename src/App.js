import React from 'react';

// Use at the root of your app
import 'antd/dist/antd.css'; 
import './App.css';
import ComplaintList from './components/user_components/ComplaintList';
import { Layout, Menu, Breadcrumb } from 'antd';

const { Header, Content, Footer } = Layout;


const LayoutComponent = (props) =>{
    return(
  <Layout className="layout" style={{background:"white"}}>
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu>
      </Header>
      <div className="main_content">     
    <Content style={{ padding: '0 50px' }}>
        <div className="site-layout-content">
          {props.children}
        </div>
      </Content>
      </div>
    </Layout>
  
    )
  }

function App() {
    return (
        <div className="App">
            <LayoutComponent>
            <ComplaintList />
            </LayoutComponent>
        </div>
    );
}




export default App;
