import { Collapse, Button, Tabs, Form, Input, InputNumber } from 'antd';
import { EditFilled } from '@ant-design/icons';
import React, { useState } from 'react';

const {TabPane} = Tabs;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const UserDetailForm = (props) => {

  const [edit,setEdit] = useState(false)
  const onFinish = values => {
    console.log(values);
  };

  return (
    <Form {...layout} name="nest-messages" initialValues={{user:{address:"12, Saravanampatti, KCT"}}} onFinish={onFinish}>
      <Form.Item name={['user', 'name']} label="Name" rules={[{ required: true }]}>
        <Input disabled={!edit} defaultValue="Name" />
      </Form.Item>
      <Form.Item name={['user', 'phone']} label="Phone">
        <Input disabled={!edit} defaultValue="8838034518" />
      </Form.Item>
      <Form.Item name={['user', 'address']} label="Address">
        <Input.TextArea disabled={!edit} defaultValue={""} />
      </Form.Item>
      
      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
        <Button disabled={!edit} type="primary" htmlType="submit">
          Submit
        </Button>
        
      <Button onClick={()=>{setEdit(!edit)}}>
        <EditFilled />{edit?"Cancel":"Edit"}
      </Button>
      </Form.Item>
    </Form>
  );
};
function ComplaintList() {
  

  
  const { Panel } = Collapse;
  function callback(key) {
    console.log(key);
  }
  const texts = [`This is complaint 1.`,`This is complaint 2.`,`This is complaint 3.`];
  return (
    <Collapse accordion defaultActiveKey={['1']} onChange={callback}>
      {texts.map(function(text,index){
        return(
          <Panel header={`Complaint ${index+1}`}  key={`${index+1}`}>
        <Tabs defaultActiveKey="1" onChange={callback}>
          <TabPane tab="User Info" key="1">
            <UserDetailForm />
        </TabPane>
          <TabPane tab="Complaint" key="2">
            Content of Tab Pane 2
          </TabPane>
        </Tabs>
        <p>{text}</p>
      </Panel>
        )
      })}
      
    </Collapse>)
}







export default ComplaintList;